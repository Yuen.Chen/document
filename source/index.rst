.. cdp-api-docs documentation master file, created by
   sphinx-quickstart on Thu Aug 13 16:33:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cdp-api-docs's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
